import copy


def sum_row_left(row):
    n = len(row)
    for i in range(0, n-1):
        if row[i] != 0 and row[i] == row[i+1]:
            row[i] = row[i] + row[i+1]
            row[i+1] = 0
    return row


def offset_row_left(row):
    n = len(row)
    for i in range(0,n-1):
        if row[i] == 0:
            # Count the number of zeroes to the right of the tile (variable j)
            for j in range(i, n):
                # Verify if there is any non-zero tile in the way
                if row[j] != 0:
                    break

            if row[j] != 0:
                row[i], row[j] = row[j], row[i]

    return row


def move_row_left(row):
    row = offset_row_left(row)
    row = sum_row_left(row)
    row = offset_row_left(row)
    return row


def move_row_right(row):
    row.reverse()
    row = move_row_left(row)
    row.reverse()
    return row


def transpose_grid(grid):
    grid_t = []
    grid_line = []
    for i in range(len(grid[0])):
        for row in grid:
            grid_line.append(row[i])

        grid_t.append(grid_line)
        grid_line = []
    return grid_t


def move_grid(grid, direction):
        if direction == "left":
            for row in grid:
                move_row_left(row)
        elif direction == "right":
            for row in grid:
                move_row_right(row)
        elif direction == "up":
            grid_t = transpose_grid(grid)
            for row in grid_t:
                move_row_left(row)
            grid = transpose_grid(grid_t)
        elif direction == "down":
            grid_t = transpose_grid(grid)
            for row in grid_t:
                move_row_right(row)
            grid = transpose_grid(grid_t)

        return grid


def is_grid_full(grid):
    is_full = True
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == 0:
                is_full = False

    return is_full


def equal_grids(grid1, grid2):
    result = False
    if len(grid1) == len(grid2) and len(grid1[0]) == len(grid2[0]):
        result = True
        for i in range(len(grid1)):
            for j in range(len(grid1[0])):
                if grid1[i][j] != grid2[i][j]:
                    return False

    return result


def move_possible(grid):
    directions = ["left", "up", "right", "down"]
    result_list = []  # In order: [Left, Up, Right, Down]
    # Create a deep copy of the grid so that the test moves do not change the original grid
    grid_to_modify = copy.deepcopy(grid)

    for direction in directions:
        grid_to_modify = move_grid(grid_to_modify, direction)

        # If the grid after the movement is identical to it before the movement, the movement is not possible
        if equal_grids(grid, grid_to_modify):
            result_list.append(False)
        else:  # Otherwise movement is possible
            result_list.append(True)
            # Undo the movement for the next test
            grid_to_modify = copy.deepcopy(grid)

    return result_list



#print(move_row_left([2, 4, 8, 16]))
#print(move_possible([[2,0,0,0], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 16, 0]]))
#print(move_possible([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 16, 0]]))
#print(move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]))

#print(move_row_right([0, 2, 0, 4]))
#grid = move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"up")
#grid = move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"down")
#print(grid)

