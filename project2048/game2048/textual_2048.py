from game2048.grid_2048 import *

THEMES = {"0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"},
          "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"},
          "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


def read_player_command():

    repeat = True
    while repeat:
        move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
        print(move)
        move = move.lower()

        repeat = False
        if move != "g" and move != "d" and move != "h" and move != "b":
            print("Commande invalide !")
            repeat = True

    if move == "g":
        return "left"
    elif move == "d":
        return "right"
    elif move == "h":
        return "up"
    elif move == "b":
        return "down"

    #return move


def read_size_grid():
    repeat = True
    while repeat:
        size = input("Entrez la taille de votre grille :")
        size = int(size)
        repeat = False
        if size <= 1:
            repeat = True
            print("Nombre invalide !")
    return size


def read_theme_grid():
    repeat = True
    while repeat:
        print("Sélectionnez le thème :")
        print("0 : Classique")
        print("1 : Éléments chimiques")
        print("2 : Alphabét")
        theme_string_number = input("Option : ")
        repeat = False
        if theme_string_number != "0" and theme_string_number != "1" and theme_string_number != "2":
            repeat = True
            print("Option invalide !")

    return theme_string_number


def game_play():
    print("__Game 2048__")
    size = read_size_grid()
    theme_key = read_theme_grid()
    theme = THEMES[theme_key]
    print("Initialization de jeu !")

    game_over = False
    game_grid = init_game(size)
    while not game_over:  # Game loop

        print(grid_to_string_with_size_and_theme(game_grid, theme, size))
        direction = read_player_command()

        # Verify if the movement changes the grid
        movements_allowed_list = move_possible(game_grid)
        verifying_index = 0
        if direction == "left":
            verifying_index = 0
        elif direction == "up":
            verifying_index = 1
        elif direction == "right":
            verifying_index = 2
        elif direction == "down":
            verifying_index = 3

        # If the movement given by the user is valid (changes the grid):
        if movements_allowed_list[verifying_index]:
            # Move the grid
            game_grid = move_grid(game_grid, direction)

            # Verify if it's game over (To decide if it should add a new random tile)
            game_over = is_game_over(game_grid)
            if not game_over:
                # If the grid was altered, add a new random tile
                grid_add_new_tile(game_grid)

            # Verify if it's game over again (To really verify if it is game over)
            game_over = is_game_over(game_grid)
            max_value = get_grid_tile_max(game_grid)

            if max_value == 2048:
                print("Félicitations ! Vous avez une configuration gagnante !")
        # Otherwise, do nothing

    # Print the grid one more time to show the game over grid
    print(grid_to_string_with_size_and_theme(game_grid, theme, size))
    print("Jeu terminé !")


# main execution #
if __name__ == '__main__':
    game_play()
    exit(1)
