
Project made during the 2018 Coding Week of CentraleSupélec, to create a clone of the game 2048 released in 2014 by Gabriele Cirulli. The purpose of this project is to develop the following concepts: 

* Agile software development;

* Use of Git and the concept of version control software;

* Python and general logic.



## Tools used

* Programming Language: Python 3.6

* IDE: PyCharm 2018.2 (Edu)



## Authors


* **Lucas Akira** - GitHub: [lucas-akira](https://github.com/lucas-akira), GitLab: [2018morishitl](https://gitlab-student.centralesupelec.fr/2018morishitl)

* **Robinson Beaucour** - GitLab: [2018beaucourr](https://gitlab-student.centralesupelec.fr/2018beaucourr)



## Acknowledgments
* README based on template made by [PurpleBooth](https://github.com/PurpleBooth) available at: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2

